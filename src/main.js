// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import focus from './directives/focus.vue'

Vue.config.productionTip = false
/* GLOBAL DIRECTIVES */
Vue.directive('focus', focus)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
